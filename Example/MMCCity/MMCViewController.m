//
//  MMCViewController.m
//  MMCCity
//
//  Created by Joker on 11/16/2018.
//  Copyright (c) 2018 广东灵机文化传播有限公司（本内容仅限于广东灵机文化传播有限公司内部传阅，禁止外泄以及用于其他的商业目的）. All rights reserved.
//

#import "MMCViewController.h"
#import <MMCCityPickerView.h>

@interface MMCViewController ()

@end

@implementation MMCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"选择城市" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn sizeToFit];
    btn.center = self.view.center;
    [btn setBackgroundColor:[UIColor blueColor]];
    [btn addTarget:self action:@selector(testBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    
}

- (void)testBtn:(UIButton *)sender {
    
    [MMCCityPickerView showPickerWithView:self.view withArea:nil withAddress:nil complete:^(NSString *area, NSString *address) {
        NSLog(@"%@,%@", area, address);
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
