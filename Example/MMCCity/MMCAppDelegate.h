//
//  MMCAppDelegate.h
//  MMCCity
//
//  Created by Joker on 11/16/2018.
//  Copyright (c) 2018 广东灵机文化传播有限公司（本内容仅限于广东灵机文化传播有限公司内部传阅，禁止外泄以及用于其他的商业目的）. All rights reserved.
//

@import UIKit;

@interface MMCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
