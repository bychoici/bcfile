# MMCChineseConvert

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MMCChineseConvert is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MMCChineseConvert'
```

## Author

Joker, xuzerong@linghit.com

## License

MMCChineseConvert is available under the MIT license. See the LICENSE file for more info.

## Usage

```
#import <MMCChineseConvert/NSObject+MMCLanguage.h>

    NSString *eg = @"日历历史日曆歷史";
    NSString *simp = [eg toSimp]; // 将eg转换为简体 --> simp = 日历历史日历历史
    NSString *tran = [eg toTran]; // 将eg转换为繁体 --> tran = 日曆曆史日曆歷史
    NSString *local = [eg toLocal]; // 根据本地语言环境，将eg转换为简体或繁体

```